import os
import sys
import unittest

sys.path.append(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from text_proc import *


class TestTextProcessingFunctions(unittest.TestCase):
    def test_do_text_replacements(self):
        text = "Gordon, as e'er, tried to make difficulties."

        exp = "Gordon, as ever, tried to make difficulties."
        got = do_text_replacements(text)

        self.assertEqual(got, exp)

    def test_lexical_diversity(self):
        words = []
        got = lexical_diversity(words)
        self.assertEqual(got, None)

        words = ['a']
        got = lexical_diversity(words)
        self.assertEqual(got, 1.0)

        words = 100 * ['a']
        got = lexical_diversity(words)
        self.assertEqual(got, 0.01)

        words = ['a', 'b', 'c', 'd', 'e']
        got = lexical_diversity(words)
        self.assertEqual(got, 1.0)

        words = 2 * ['a', 'b', 'c', 'd', 'e']
        got = lexical_diversity(words)
        self.assertEqual(got, 0.5)

        words = ['a', 'a', 'b', 'c', 'd', 'e']
        got = lexical_diversity(words)
        self.assertEqual(got, 0.83)

        words = ['a', 'a', 'a', 'b', 'c', 'd', 'e']
        got = lexical_diversity(words)
        self.assertEqual(got, 0.71)

        words = ['a', 'a', 'b', 'b', 'c', 'd', 'e']
        got = lexical_diversity(words)
        self.assertEqual(got, 0.71)

    def test_percentage(self):
        self.assertEqual(percentage(2, None), None)
        self.assertEqual(percentage(0, 10), 0.)
        self.assertEqual(percentage(2, 10), 20.)
        self.assertEqual(percentage(10, 10), 100.)
        self.assertEqual(percentage(11, 10), 110.)

    def test_get_tokens(self):
        text = ''
        exp = []
        self.assertEqual(get_tokens(text), exp)

        text = 'This'
        exp = ['this']
        self.assertEqual(get_tokens(text), exp)

        text = "This grief's without reason."
        exp = ['this', 'grief', "'s", 'without', 'reason', '.']
        self.assertEqual(get_tokens(text), exp)

    def test_get_words(self):
        tokens = []
        exp = []
        self.assertEqual(get_words(tokens), exp)

        tokens = ['I', 'have', '2', 'dogs']
        exp = ['I', 'have', 'dogs']
        self.assertEqual(get_words(tokens), exp)

        tokens = ['this', 'grief', "'s", 'without', 'reason', '.']
        exp = ['this', 'grief', 'without', 'reason']
        self.assertEqual(get_words(tokens), exp)

        # 'st' from archaic words like "be'st" is excluded
        tokens = ['If', 'thou', "be'st", "born"]
        exp = ['If', 'thou', 'born']
        self.assertEqual(get_words(tokens), exp)

    def test_get_most_common_words(self):
        words = []
        exp = []
        self.assertEqual(get_most_common_words(words), exp)

        words = ['mother']
        exp = []
        self.assertEqual(get_most_common_words(words), exp)

        words = ['mother', 'mother']
        exp = [('mother', 2)]
        self.assertEqual(get_most_common_words(words), exp)

        words = ['mother', 'father', 'father', 'son']
        exp = [('father', 2)]
        self.assertEqual(get_most_common_words(words), exp)

        words = ['mother', 'father', 'father', 'son', 'son', 'son', 'I']
        exp = [('son', 3), ('father', 2)]
        self.assertEqual(get_most_common_words(words), exp)

        # step words are excluded
        words = ['father', 'father'] + [STOPWORDS[0]]
        exp = [('father', 2)]
        self.assertEqual(get_most_common_words(words), exp)

    def test_get_stem_words(self):
        words = []
        exp = []
        self.assertEqual(get_stem_words(words), exp)

        words = ['fishing', 'fished', 'fisher', 'fish']
        exp = ['fish', 'fisher']
        self.assertEqual(get_stem_words(words), exp)


if __name__ == '__main__':
    unittest.main()
