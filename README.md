# Poems NLP

## Instalation

### Requirements
- Python 3 (tested on Python 3.6.4)
    ``brew install python3``

I had some problems linking python3
    ``sudo mkdir /usr/local/Frameworks
    sudo chown $(whoami):admin /usr/local/Frameworks
    brew link python3``

- pip 9.0.1
    ``brew postinstall python3``


### Setup
``git clone git@gitlab.com:dbsima/poems-nlp.git
cd poems-nlp/
pip3 install virtualenv
virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt
python setup.py``

## Run
`` python poems.py``
