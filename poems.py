# coding: utf-8

import os

from text_proc import *


for root, dirs, files in os.walk("poems"):
    for f in files:
        if f.endswith(".txt"):
            print()
            print(100 * '-')
            print(f)
            print(len(f) * '-')
            path = os.path.join(root, f)

            poem = open(path).read()

            poem = do_text_replacements(poem)

            tokens = get_tokens(poem)
            print()
            print('Number of tokens', len(tokens))

            words = get_words(tokens)
            print()
            print('Number of words', len(words))

            stem_words = get_stem_words(words)
            print('Number of words', len(stem_words), '(stem)')

            ld = lexical_diversity(words)
            print()
            print('Lexical diversity', ld)

            print()
            mc_words = get_most_common_words(words)
            print('Most common words', mc_words)
