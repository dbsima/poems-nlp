# coding: utf-8

import string
import nltk


STOPWORDS = nltk.corpus.stopwords.words('english')
STOPWORDS.extend(['thee', 'thou', 'ye', 'thy', 'thine', 'thyself'])


WORDS_REPLACEMENTS = [
    ("e’er", "ever"),
    ("e'er", "ever"),
    ("o’er", "over"),
    ("o'er", "over"),
    ("howe’er", "however"),
    ("howe'er", "however"),
    ("o’erhead", "overhead"),
    ("o'erhead", "overhead"),
]


def do_text_replacements(text):
    """ Return the passed <text> after the replacements have been done.
    """
    for (word, replacement) in WORDS_REPLACEMENTS:
        text = text.replace(word, replacement)

    return text


def lexical_diversity(tokens):
    """ Return the lexical diversity of a list of tokens.

        The number returned is rounded to 2 decimal places.
    """
    if not tokens:
        return None

    return round(len(set(tokens)) / len(tokens), 2)


def percentage(count, total):
    """ Return the percentage of <count> in <total>.
    """
    if not total:
        return None

    return 100 * count / total


def get_tokens(text):
    """ Return the list of tokens (in lower case) from the passed <text>.
    """
    tokens = nltk.word_tokenize(text)
    tokens = [token.lower() for token in tokens]
    return tokens


def get_words(tokens):
    """ Return the list of words from the passed <tokens>

        A token is considered a word if it consists of alphabetic
        characters only and it is not 'st' (from "think’st", "swell’st").
    """

    def keep_token(token):
        if not token.isalpha():
            return False

        if token == 'st':
            # from "think’st", "swell’st", ...
            return False

        return True

    return [token for token in tokens if keep_token(token)]


def get_most_common_words(words):
    """ Return the list of the most common words with their corresponding
        number of occurences.

        A number is considered 'most common' if it is not a stop word and
        it appears more than one time.
    """

    # filter out stop words
    _words = [word for word in words if word not in STOPWORDS]

    # get the words and their occurences
    fdist = nltk.probability.FreqDist(_words)
    fdist_mc_words = fdist.most_common()

    # filter our words with one occurence
    fdist_mc_words = [t for t in fdist_mc_words if t[1] > 1]

    return fdist_mc_words


def get_stem_words(words):
    """ Return the list of stem words.

        Stemming refers to the process of reducing each word to its root
        or base.

        For example “fishing,” “fished,” “fisher” all reduce to the stem
        “fish.”

        The stemming algorithm of choice is calledPorter Stemming.
    """
    porter = nltk.stem.porter.PorterStemmer()
    return sorted(set([porter.stem(word) for word in words]))
